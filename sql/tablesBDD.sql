
#------------------------------------------------------------
# Table: utilisateur
#------------------------------------------------------------

CREATE TABLE utilisateur(
        id_utlisateur  Int  Auto_increment  NOT NULL ,
        nom            Varchar (50) NOT NULL ,
        email          Varchar (50) NOT NULL ,
        mot_de_passe   Varchar (50) NOT NULL ,
        administrateur Bool
	,CONSTRAINT utilisateur_PK PRIMARY KEY (id_utlisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: article
#------------------------------------------------------------

CREATE TABLE article(
        id_article     Int  Auto_increment  NOT NULL ,
        titre_article  Varchar (100) NOT NULL ,
        date_article   Date NOT NULL ,
        publier        Bool ,
        nb_like        Int ,
        txt1           Varchar (1000) NOT NULL ,
        txt2           Varchar (1000) NOT NULL ,
        txt3           Varchar (1000) NOT NULL ,
        img_url        Varchar (50) NOT NULL ,
        arrosage       Varchar (50) NOT NULL ,
        ensoleillement Varchar (50) NOT NULL ,
        id_utlisateur  Int NOT NULL
	,CONSTRAINT article_PK PRIMARY KEY (id_article)

	,CONSTRAINT article_utilisateur_FK FOREIGN KEY (id_utlisateur) REFERENCES utilisateur(id_utlisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: commentaire
#------------------------------------------------------------

CREATE TABLE commentaire(
        id_commentaire   Int  Auto_increment  NOT NULL ,
        note             Int ,
        contenu          Varchar (300) NOT NULL ,
        date_commentaire Date NOT NULL ,
        id_utlisateur    Int NOT NULL ,
        id_article       Int NOT NULL
	,CONSTRAINT commentaire_PK PRIMARY KEY (id_commentaire)

	,CONSTRAINT commentaire_utilisateur_FK FOREIGN KEY (id_utlisateur) REFERENCES utilisateur(id_utlisateur)
	,CONSTRAINT commentaire_article0_FK FOREIGN KEY (id_article) REFERENCES article(id_article)
)ENGINE=InnoDB;

