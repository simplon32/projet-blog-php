<?php
class Database{
    const DB_HOST='localhost';
    const DB_BASE='projet_blog';
    const DB_USER='blogUser';
    const DB_MDP='blogUser';
    private $_BDD;

    public function __construct(){
        $this->connectBDD();
    }

    private function connectBDD(){
        try {
            $this->_BDD = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_MDP, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (PDOException $e){
            echo 'Connexion échouée'. $e-> getMessage();
        }
    }

    public function getAllArticles(){
    $sql = 'SELECT * FROM article ORDER BY date_article DESC';
    $requete =  $this->_BDD->query($sql);
    $resultat = $requete->fetchAll(PDO::FETCH_OBJ);

    return $resultat;
  }
    
  public function getAllUser(){
    $sql = 'SELECT * FROM utilisateur';
    $requete =  $this->_BDD->query($sql);
    $resultat = $requete->fetchAll(PDO::FETCH_OBJ);

    return $resultat;
  }
        
    public function getBDD(){
        return $this->_BDD;
      }
    
    function supprimer_table() {
        $query = "DROP TABLE IF EXISTS utilisateur";
        $this->_BDD->query($query);
    }
}

?>