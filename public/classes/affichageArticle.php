<?php

?>
<main class="main" id="main">
    <section class="article-container">
        <article class="article_1 article_all">
        <?php $db = new Database;
            $articles = $db->getAllArticles();
            foreach($articles as $article){ ?>
            <h2><?= $article->titre_article; ?></h2>
            <div class="date">
                <p>publié le <?= $article->date_article; ?></p>
            </div>
            <p><?= $article->txt1; ?></p>
            <img src="<?= $article->imgUrl; ?>" />
            <p><?= $article->txt2; ?><?= $article->txt2; ?></p>
            <div class="conseil">
                <div class="conseil1">
                    <img src="./assets/images/soleil.svg" class="soleil" />
                    <p><?= $article->ensoleillement; ?></p>
                    </div>
                <div class="conseil2">
                    <img src="./assets/images/arrosage.svg" class="arrosage" />
                    <p><?= $article->arrosage; ?></p>
                    </div>
            </div>

            <p><?= $article->txt3; ?></p>
            <?php } ?>
            <span class="nom_utilisateur">Jeanne&nbsp;d'Arc</span>

            <div class="commentaire">
                <span class="like fas fa-thumbs-up"></span>
                <h3>Commentaires</h3>
            </div>
        </article>
    </section>
</main>