<?php
class Article{
    private int $_idArticle;
    private string $_titre;
    private string $_date;
    private bool $_publie;
    private int $_nbLike;
    private string $_txt1;
    private string $_txt2;
    private string $_txt3;
    private string $_imgUrl;
    private string $_arrosage;
    private string $_ensoleillement;

// Constructeur & destructeur
    public function __construct(array $contenu = array()){
        $this-> hydrate($contenu);

    }
    private function hydrate(Array $contenu){
            foreach ($contenu as $key =>$value){
                $method = 'set' .ucfirst($key);
                if(method_exists($this, $key)){
                    $this->$method($value);
                }
            }
    }
    public function __destruct(){
    }

//Getters & Setters
    public function getTitre()
    {
        return $this->_titre;
    }

    public function setTitre($_titre)
    {
        $this->_titre = $_titre;

        return $this;
    }


    public function getIdArticle()
    {
        return $this->_idArticle;
    }


    public function setIdArticle($_id)
    {
        $this->_idArticle = $_id;

        return $this;
    }

   
    public function getDate()
    {
        return $this->_date;
    }

  
    public function setDate($_date)
    {
        $this->_date = $_date;

        return $this;
    }

   
    public function getPublie()
    {
        return $this->_publie;
    }


    public function setPublie($_publie)
    {
        $this->_publie = $_publie;

        return $this;
    }

    public function getNbLike()
    {
        return $this->_nbLike;
    }

    public function setNbLike($_nbLike)
    {
        $this->_nbLike = $_nbLike;

        return $this;
    }

    public function getTxt1()
    {
        return $this->_txt1;
    }

    public function setTxt1($_txt1)
    {
        $this->_txt1 = $_txt1;

        return $this;
    }

    public function getTxt2()
    {
        return $this->_txt2;
    }

    public function setTxt2($_txt2)
    {
        $this->_txt2 = $_txt2;

        return $this;
    }

    public function getTxt3()
    {
        return $this->_txt3;
    }

    public function setTxt3($_txt3)
    {
        $this->_txt3 = $_txt3;

        return $this;
    }

    public function getImgUrl()
    {
        return $this->_imgUrl;
    }

    public function setImgUrl($_imgUrl)
    {
        $this->_imgUrl = $_imgUrl;

        return $this;
    }

    public function getArrosage()
    {
        return $this->_arrosage;
    }

    public function setArrosage($_arrosage)
    {
        $this->_arrosage = $_arrosage;

        return $this;
    }

    public function getEnsoleillement()
    {
        return $this->_ensoleillement;
    }

    public function setEnsoleillement($_ensoleillement)
    {
        $this->_ensoleillement = $_ensoleillement;

        return $this;
    }
}