<?php
if (isset($_POST['deconnecte'])) {
    // Si oui, on supprime la session :
    session_destroy();
        echo "session deconnectée";
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" /> -->
    <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/x-icon" />
    <title>Blog Plantes</title>
    <link rel="stylesheet" href="./assets/css/font-awesome.css" />
    <link rel="stylesheet" href="./assets/css/style_new.css" />
</head>

<body>
    <div class="grid_container">
        <header class="header_flex">
            <div class="logo">
                <img src="./assets/images/lys.svg" alt="logo" />
            </div>

            <div class="slogan-flex">
                <h1 class="slogan">
                    Bienvenue <?php if ($_SESSION['connect'] == true) {
                               $nom= $_SESSION['user_actuel']->getNom();
                               echo $nom;
                               
                            }?>
                            sur le blog référence pour chouchoutez vos plantes.
                </h1>
            </div>

            <div class="header_content">
                <!-- <div class="header_link"> -->
                <div class="connecter">
                    <?php if ($_SESSION['connect'] == false){?>                      
                        <a href="connexion.php">se connecter
                    <?php }
                    else if($_SESSION['connect']){?>
                        <form action="#" method="POST">
                            <button type="submit" name="deconnecte">déconnecter</button>
                        </form>
                        ?>
                    <?php } ?>
                </div>

                <div class="inscrire">
                    <a href="inscription.php">s'inscrire</a>
                </div>
                <!-- </div> -->
            </div>
        </header>