<?php
require "init.php";
?>
include "header.php";
include "aside.php";
include "footer.php";
<main class="main" id="main">
    <section class="article-container">
        <article class="article_1>
        <?php $articleRepo = new ArticleRepository;
        $article = $articleRepo-> getArticle($_SESSION['article']);
        ?>
<h2><?= $article->getTitre(); ?></h2>
            <div class="date">
                <p>publié le <?= $article->getDate(); ?></p>
            </div>
            <p><= $article->getText1(); ?></p>
            <img src="<?= $article->getImgUrl(); ?>" />
            <p><= $article->getText2(); ?></p>
            <div class="conseil">
                <div class="conseil1">
                    <img src="./assets/images/soleil.svg" class="soleil" />
                    <p><= $article->getEnsoleillement(); ?></p>
                    </div>
                <div class="conseil2">
                    <img src="./assets/images/arrosage.svg" class="arrosage" />
                    <p><= $article->getArrosage(); ?></p>
                    </div>
            </div>
            <p><= $article->getText3(); ?></p>