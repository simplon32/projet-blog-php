# Mon blog Personnel | From Scratch
***
Un blog autour des plantes.


###Objectifs du projet
***
Réaliser un projet en binôme pour exercer la communication, l’organisation, l’échange du savoir-faire et le transfert de fichiers via GitLab.com et d’autres moyens informatiques en travaillant sur le même code.

Notre page web est un blog dans lequel l'utilisateur peut se connecter afin d'accéder à l'option d'écriture de commentaire mais s’il préfère il peut consulter le site et accéder aux articles et la visualisation des commentaires des autres utilisateur sans se connecter.

Si l’utilisateur qui se connecte est administrateur celui-ci pourra créer de nouveaux articles et ajouter de nouveaux utilisateurs.


###Réalisations de notre projet :
***
Nous avons une page accueil qui affiche des extraits de tous les articles triés par ordre de date de publication. Sur celle-ci il y a un lien pour lire l’article choisi en entier. 

L'utilisateur peut s’inscrire et en suite se connecter en cliquant sur les boutons prévu pour chaque de cette occasion.


###Technologie utilisée:
***
L’organisation du projet a été faite sur trello.com
Les maquettes et wireframes pour les mobiles et la version écran desktop ont été réalisés sur www.figma.com et ils sont été exportés en PDF.

Le codage du projet a été réalisé avec l’éditeur de code Visual Studio Code.

La partie statique et adaptative du frontend compte plusieurs fichiers.php avec un contenu structuré en HTML5 et la mise en page en CSS3.

Pour les icônes utilisées : les fichiers nécessaires de Font Awesome ont été intégré dans le dossier du projet.

La partie backend est codée en langage informatique PHP et SQL pour assurer : 
    • l’accès au serveur MYSQL.,
    • l’échange de données avec la base de données,
    • le dynamisme du site web.

Nous avons codé dans le langage PHP en utilisant la POO et le PDO.


###Installation
***
Le dossier entier de ce site web doit être téléverser sur un serveur d’un hébergeur web pour assurer ses fonctionnalités. Dans ce cas il est obligatoire, entre autre, d’ajouter un certificat SSL (https au lieu de http) pour augmenter la sécurité et la protection des données personnelles.

Il est aussi possible d’installer un serveur local, par exemple un Apache serveur Web, sur son ordinateur ou son ordinateur portable
Pour le système d’exploitation de Windows 10 on peut installer le logiciel WAMP qui offre aussi l’environnement pour développer des base de données sur MYSQL le serveur de base de données.

WAMP : Windows, Apache, MySQL/MariaDB, PHP
Plus d’information : https://www.wampserver.com/

Mamp : Macintosh, Apache, MySQL/MariaDB, PHP
Plus d’information : https://documentation.mamp.info/

LAMP :  Linux, Apache, MySQL, PHP
Plus d’information : https://doc.ubuntu-fr.org/lamp

Pour la visualisation des pages un navigateur web, acceptant les cookies et JavaSript activée, est également nécessaire.


###Remerciement
*** 
Grand merci à Monsieur Théophile Bellintani, formateur à Simplon, pour débugger les situations difficiles d’une manière professionnelle.


### Glossaire
***
Wireframe : le wireframe, ou maquette fonctionnelle en français, est le schéma d’une page web ou d’une application.

PDF : Portable Document Format.

HTML5 : HyperText Markup Language (version 5), est le code utilisé pour structurer une page web. 

CSS3 : Cascading Style Sheets (version 3), est un langage informatique utilisé sur Internet pour la mise en forme de fichiers et de pages HTML. 

PHP : Hypertext Preprocessor est un langage de script côté serveur. Permettant tout à la fois la production de pages web dynamiques et la communication avec le serveur MySQL.

SQL : Structured Query Language, est un langage de programmation permettant de manipuler les données et les systèmes de bases de données relationnelles. 

MYSQL : My Structured Query Language, désigne un serveur de base de données distribué sous licence libre GNU (General Public License). 

SSL : Secure Socket Layer, signifie couche des sockets. Il sécurise les données des utilisateurs, vérifie qui est le propriétaire du site Web, empêcher les criminels de créer une version frauduleuse du site et obtenir la confiance des utilisateurs Un certificat SSL n’est pas gratuit. 

L'utilisation d’HTTPS pour l'accès à votre blog présente trois avantages principaux par rapport à HTTP:
    • elle permet de vérifier si le visiteur ouvre le site Web      adéquat et s'il n'est pas redirigé vers un site malveillant, 
    • elle aide à détecter si un pirate tente de modifier des données envoyées au visiteur depuis blogger, 
    • elle apporte des mesures de sécurité qui empêchent d'autres personnes d'écouter les conversations de vos visiteurs, de suivre     leurs activités ou de voler leurs informations.

HTTP : Hypertext Transfer Protocol est l'ensemble de règles régissant le transfert de fichiers (texte, images, son, vidéo, et autres fichiers multimédias) sur le Web. 

HTTPS : Hyper Text Transfer Protocol Secure est une extension sécurisée du protocole HTTP, signifie que les données échangées entre le navigateur de l'internaute et le site web sont chiffrées et ne peuvent en aucun cas être espionnées (confidentialité) ou modifiées. 

Hébergeur web : un hébergeur web est une entreprise qui fournit l'hébergement sur Internet de systèmes informatiques divers. 

Font Awesome :  est une police d'écriture et un outil d'icônes qui se base sur CSS, Less et Sass.
Plus d’information : https://fontawesome.com/

