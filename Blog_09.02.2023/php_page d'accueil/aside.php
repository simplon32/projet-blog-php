<?php

?>



<aside class="aside">
    <div>
        <input type=" text" value="Recherche" id="recherche" />
        <label for="recherche"><span class="fas fa-search"></span></label>
    </div>

    <h2>Sommaire</h2>

    <div class="aside_cont_flex">
        <div class="aside_content">
            <h3>Article 1</h3>
            <h3>Titre de l'article 1</h3>
        </div>

        <div class="aside_content">
            <h3>Article 2</h3>
            <h3>Titre de l'article 2</h3>
        </div>

        <div class="aside_content">
            <h3>Article 3</h3>
            <h3>Titre de l'article 3</h3>
        </div>
    </div>
</aside>