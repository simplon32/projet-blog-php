<main class="main" id="main">
    <section class="article-container">
        <article class="article_1 article_all">
            <h2>Article 1</h2>
            <div class="date">
                <p>publié le 19&shy; Janvier&nbsp;2023</p>
            </div>
            <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Repellendus, nostrum! Similique dolorum esse asperiores iste a
                obcaecati at quos vitae tenetur error, cumque veritatis labore
                expedita perferendis numquam molestiae fuga voluptates
                perspiciatis&nbsp;!
            </p>
            <img src="./assets/images/article1_img.png" alt="plante" />
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Cumque, doloremque quod. Magni odit voluptas et molestias
                voluptates, delectus praesentium, a eos nisi ipsa excepturi
                necessitatibus earum vero repellendus alias ! Accusamus
                reiciendis est nam enim animi, officiis pariatur, tenetur vel
                temporibus, consequatur voluptatum illum quo ! Eum deleniti
                repellendus veniam, consectetur ullam nemo illum a sint earum,
                nostrum culpa minus alias sed ad natus. Sapiente, pariatur qui
                perferendis alias earum cumque rem est a molestias explicabo
                quisquam aliquam, optio dolores, animi&nbsp;sint.
            </p>
            <div class="conseil">
                <div class="conseil1">
                    <img src="./assets/images/soleil.svg" class="soleil" />
                    <p>Plein&nbsp;Soleil</p>
                </div>
                <div class="conseil2">
                    <img src="./assets/images/arrosage.svg" class="arrosage" />
                    <p>Arrosage&nbsp;Abondant</p>
                </div>
            </div>

            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere
                suscipit repellendus reiciendis ipsa animi et ab iste expedita
                rerum&nbsp;nulla&nbsp;!
            </p>

            <span class="nom_utilisateur">Jeanne&nbsp;d'Arc</span>

            <div class="commentaire">
                <span class="like fas fa-thumbs-up"></span>
                <h3>Commentaires</h3>
            </div>
        </article>
    </section>
</main>