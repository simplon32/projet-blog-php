<?php



?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" /> -->
    <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/x-icon" />
    <title>Blog Plantes</title>
    <link rel="stylesheet" href="./assets/css/font-awesome.css" />
    <link rel="stylesheet" href="./assets/css/style_new.css" />
</head>

<body>
    <div class="grid_container">
        <header class="header_flex">
            <div class="logo">
                <img src="./assets/images/lys.svg" alt="logo" />
            </div>

            <div class="slogan-flex">
                <h1 class="slogan">
                    Bienvenue sur blog référence pour chouchoutez vos
                    plantes.
                </h1>
            </div>

            <div class="header_content">
                <!-- <div class="header_link"> -->
                <div class="connecter">
                    <a href="connecter.html">se connecter</a>
                </div>

                <div class="inscrire">
                    <a href="inscrire.html">s'inscrire</a>
                </div>
                <!-- </div> -->
            </div>
        </header>