<?php
class Utilisateur{
    private $_idUtilisateur;
    private string $_nom;
    private string $_email;
    private string $_mdp;
    private bool $_admin;
    public function __constructor(array $contenu = array()){
        $this-> hydrate($contenu);
    }
    private function hydrate(Array $contenu){
        foreach ($contenu as $key =>$value){
            $method = 'set' .ucfirst($key);
            if(method_exists($this, $key)){
                $this->$method($value);
            }
        }
}

//Getters & Setters
    public function getIdUtilisateur()
    {
        return $this->_idUtilisateur;
    }

    public function setIdUtilisateur($_idUtilisateur)
    {
        $this->_idUtilisateur = $_idUtilisateur;

        return $this;
    }

 
    public function getNom()
    {
        return $this->_nom;
    }

    public function setNom($_nom)
    {
        $this->_nom = $_nom;

        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }


    public function setEmail($_email)
    {
        $this->_email = $_email;

        return $this;
    }

  
    public function getMdp()
    {
        return $this->_mdp;
    }

 
    public function setMdp($_mdp)
    {
        $this->_mdp = $_mdp;

        return $this;
    }


    public function getAdmin()
    {
        return $this->_admin;
    }


    public function setAdmin($_admin)
    {
        $this->_admin = $_admin;

        return $this;
    }
}