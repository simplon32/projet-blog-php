<?php
class Commentaire{

    private $_idCommentaire;
    private int $_note;
    private string $_contenuComent;
    private date $_dateComent

    public function __construct(){

    }
    //getters &Setters
    
    public function get_idCommentaire()
    {
        return $this->_idCommentaire;
    }

 
    public function set_idCommentaire($_idCommentaire)
    {
        $this->_idCommentaire = $_idCommentaire;

        return $this;
    }


    public function get_note()
    {
        return $this->_note;
    }

 
    public function set_note($_note)
    {
        $this->_note = $_note;

        return $this;
    }

    public function get_contenuComent()
    {
        return $this->_contenuComent;
    }

    public function set_contenuComent($_contenuComent)
    {
        $this->_contenuComent = $_contenuComent;

        return $this;
    }
}