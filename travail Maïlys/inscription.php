<?php
require "init.php";
$userRepo = new UtilisateurRepository();

if (isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['mdpVerif'])) {
	$nom = $_POST['nom'];
	$identifiant = $_POST['email'];
	$mdp = $_POST['password'];
	$mdpVerif = $_POST['mdpVerif'];
	echo 'le mdp est' . $mdp;
	echo '<br>
la verif de mdp est' . $mdpVerif;
	echo '<br />
l identifiant est:' . $identifiant;
	if ($mdp == $mdpVerif) {
		$userRepo->createUser($nom, $identifiant, $mdp);
		exit();
	} else {
		echo "les mots de
passe ne sont pas identiques, veuillez les resaisir<br />";
	}
	if (
		$mdp ==
		$user->getMdp()
	) {
		$_SESSION['username'] = $pseudo;
		$_SESSION['connect'] = true;
		header('location:article.php');
		exit();
	} else {
		echo "erreur de mot de passe
veuillez le resaisir";
	}
} ?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Inscrivez Vous</title>
	<style>
		.inscription {
			border: 2px solid black;
			width: 340px;
			height: 400px;
			padding: 0 20px;
		}

		.form-inscrire {
			display: flex;
			flex-direction: column;
		}

		.inscription h2 {
			text-align: center;
		}

		.inscription input {
			margin: 0 0 20px;
		}

		.inscription label {
			margin: 0 0 6px;
		}

		.inscription button {
			width: 100px;
			padding: 3px 0;
		}
	</style>
</head>

<body>
	<div class="inscription">
		<h2>Inscrivez vous</h2>

		<form action="#" method="POST" class="form-inscrire" id="form-inscrire">
			<label for="nom">Nom</label>
			<input type="text" id="nom" name="nom" placeholder="nom" required />

			<label for="email">Votre email</label>
			<input type="email" id="email" name="email" placeholder="email" required />

			<label for="password">Mot de passe</label>
			<input type="password" id="password" name="password" placeholder="mot de passe" required />

			<label for="mdpVerif">Confirmez mot de passe</label>
			<input type="password" id="mdpVerif" name="mdpVerif" placeholder="mot de passe" required />

			<button id="valider ">
				<a href="confirm_inscrit.html">Valider</a>
			</button>
		</form>
	</div>
</body>

</html>