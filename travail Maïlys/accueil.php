<?php

?>
<main class="main" id="main">
    <section class="article-container">
        <article class="article_1 article_all">
        <?php $db = new Database;
            $articles = $db->getAllArticles();
            foreach($articles as $article){ ?>
            <h2><?= $article->titre_article; ?></h2>
            <div class="date">
                <p>publié le <?= $article->date_article; ?></p>
            </div>
            <img src="<?= $article->img_url; ?>" />
            <p><?php

            $textesArticle = $article->txt1 . $article->txt2; 
            $extraitArticle= substr($textesArticle, 0, 300);
            echo $extraitArticle;
            ?>
            <form action="#" method="POST">
                  <button type="submit" class="lire_suite" name="suite">Lire la suite</button>
            </form>
            <?php } 
            if (isset($_POST['suite'])) {
                echo "bouton article 1 cliqué";
                $_SESSION['article']= $article->titre_article;
                header('location:article.php');
            }
            ?></p>      
        </article>
    </section>
</main>