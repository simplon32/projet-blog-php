<?php
require "init.php";
$userRepo = new UtilisateurRepository();

if (isset($_POST['email']) && isset($_POST['password'])) {
	$identifiant = $_POST['email'];
	$mdp = $_POST['password'];
	echo 'le mdp est' . $mdp;
	echo '<br>
l identifiant est:' . $identifiant;
	$user = $userRepo->getUser($identifiant);
	var_dump($user);
    if ($user){
		if ($mdp == $user->getMdp()) {
			$_SESSION['identifiant'] = $identifiant;
			$_SESSION['connect'] = true;
			$_SESSION['user_actuel'] = $user;
			if ($user->getAdmin() == 1) {
				header('location::accueil_admin.php');
			} else {
				header('location:accueil.php');
			}
			exit();
		} else {
			echo "erreur de mot de passe veuillez le resaisir";
		}
	}
	else {echo "l'identifiant".$identifiant."n\'existe pas";}
} ?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Connecter Vous</title>
	<style>
		.connection {
			border: 2px solid black;
			width: 340px;
			height: 400px;
			padding: 0 20px;
		}

		.form-connect {
			display: flex;
			flex-direction: column;
		}

		.connection h2 {
			text-align: center;
		}

		.connection input {
			margin: 0 0 20px;
		}

		.connection label {
			margin: 0 0 6px;
		}

		.connection button {
			width: 100px;
			padding: 3px;
		}
	</style>
</head>

<body>
	<div class="connection">
		<h2>Connecter vous</h2>

		<form action="#" method="POST" class="form-connect" id="form-inscrire">
			<label for="email">Votre email</label>
			<input type="email" id="email" name="email" placeholder="email" required />

			<label for="password">Mot de passe</label>
			<input type="password" id="password" name="password" placeholder="mot de passe" required />

			<button>Valider</button>
		</form>
	</div>
</body>

</html>