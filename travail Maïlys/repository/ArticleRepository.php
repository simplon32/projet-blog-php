<?php
class ArticleRepository{
   //attributs
   private $db;

   public function __construct(){
       $this->db= new Database();
       $this->db = $this->db->getBDD();
      //  $this->db = $this->db->getBDD();
   }

   public function createArticle(string $titre,string $date, int $id_utilisateur,string $txt1, string $txt2, string $txt3, string $img_url, string $arrosage, string $ensoleillement){
        $requete = $this->db->prepare("SELECT titre_article FROM article WHERE titre_article = :titre ");
        $titrepresent= $requete->bindValue(':titre', $titre, PDO::PARAM_STR);
        $requete->execute( );
        $titrepresent = $requete->fetch(PDO::FETCH_ASSOC);
        echo 'le titre est present?' .$titrepresent;

        var_dump($titrepresent);
       
        if (!$titrepresent){
            $sql="INSERT INTO 'article'('titre_article,'date_article', 'publier', 'nb_like', 'id_utilisateur','txt1', 'txt2', 'txt3', 'img_url', 'arrosage', 'ensoleillement'  )
            VALUES (:titre_article, :date_article, 0, 0, :id_utilisateur, :txt1, :txt2, :txt3, :img_url, :arrosage, :ensoleillement)";
            $requete = $this->db->prepare($sql);
            
            $requete->execute([':titre_article'=>$titre,
                                ':date_article'=>$date,
                                ':id_utilisateur'=>$id_utilisateur,
                                ':txt1'=>$txt1,
                                ':txt2'=>$txt2,
                                ':txt3'=>$txt3,
                                ':img_url'=>$img_url,
                                ':arrosage'=>$arrosage,
                                ':ensoleillement'=>$ensoleillement              
            ]);
            echo "L'article a été ajouté à la Base de Données.";
        }
        else if($titrepresent['titre_article'] == $titrepresent){
          return "<p>Le titre d'article est déjà utilisé </p>";
      }
   }
   
   public function getArticle($article){
    if(is_string($article)){
      $sql= "SELECT * FROM article WHERE titre_article = :article ;";
    }
    else{
      $sql = "SELECT * FROM article WHERE id_article = :article ;";
    }
    $requete = $this->db->prepare($sql);
    $requete -> execute([':article' => $article]);
    $infos = $requete->fetch(PDO::FETCH_ASSOC);

    $article= [];
    foreach($infos as $key => $value){
        $key = explode('_', key);
        $key = $key[0];
        $article = array_merge($article,[$key => $value]);
      }
    
      $objetArticle = new Article($article);
      return $objetArticle;
    }
   
  // public function setArticle(Article $article){

  // }

   public function getTitre(){
    $sql = 'SELECT titre_article FROM article';
    $requete =  $this->db->query($sql);
    $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
   }

   public function getDate($articleNom){
    if (is_string($articleNom)) {
        $sql = "SELECT date_article FROM article WHERE date_article = :articleNom ;";
      }else{
        $sql = "SELECT date_article FROM article WHERE Id_article = :articleNom ;";
      }
    $requete = $this->db->prepare($sql);

    $requete->execute([':articleNom'=>$articleNom]);

    $date = $requete->fetch(PDO::FETCH_ASSOC);
    return $date['date_article'];
   }
}
?>